function isValid(input) {
    // Validate the length (max 10 characters)
    if (input.length > 10) {
      return false;
    }
  
    // Validate alphanumeric characters with 8 numbers and 1 letter (unordered)
    const regex1 = /^(?=(?:\D*\d){8})(?=(?:[^\d]*\D){1})[A-Za-z\d]{1,10}$/;

    // Validate alphanumeric characters with 9 numbers and 1 letter (unordered)
    const regex2 = /^(?=(?:\D*\d){9})(?=(?:[^\d]*\D){1})[A-Za-z\d]{1,10}$/;

    // Validate alphanumeric characters with 8 numbers and 2 letters (unordered)
    const regex3 = /^(?=(?:\D*\d){8})(?=(?:[^\d]*\D){2})[A-Za-z\d]{1,10}$/;
  
    return regex1.test(input) || regex2.test(input) || regex3.test(input);
  }
  
  module.exports = isValid;