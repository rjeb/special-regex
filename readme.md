# Regex with unit test 

Building simple regex function with unit test for js 

## Installation

Use the package manager [npm].

```bash
npm install 
```

## install and launch unit test for js

```bash
npm install jest --save-dev

# run unit test with jest
npx jest

```
