const isValid = require('./regex.js');
describe('isValid', () => {
    it('should return false for input with more than 10 characters', () => {
      expect(isValid('12345678901')).toBe(false);
    });
  
    it('should return true for input with 8 numbers and 1 letter', () => {
      expect(isValid('12345678a')).toBe(true);
    });
  
    it('should return true for input with 9 numbers and 1 letter', () => {
      expect(isValid('123456789a')).toBe(true);
    });
  
    it('should return true for input with 8 numbers and 2 letters (unordered)', () => {
      expect(isValid('a123b45678')).toBe(true);
    });
  
    it('should return false for input without a letter', () => {
      expect(isValid('1234567890')).toBe(false);
    });
  
    it('should return false for input without enough numbers', () => {
      expect(isValid('abcdefghi')).toBe(false);
    });
  
    it('should return false for input with special characters', () => {
      expect(isValid('12345678!')).toBe(false);
    });

    it('should return true for input with 10 characters (9 numbers and 1 letter)', () => {
      expect(isValid('123456789a')).toBe(true);
    });

    it('should return false for input with only 1 character (letter)', () => {
      expect(isValid('a')).toBe(false);
    });
    
    it('should return false for input with only 1 character (number)', () => {
      expect(isValid('1')).toBe(false);
    });
  
    it('should return false for input with only 2 characters (2 letters)', () => {
      expect(isValid('ab')).toBe(false);
    });
    
    it('should return false for input with only 2 characters (2 numbers)', () => {
      expect(isValid('12')).toBe(false);
    });
    
    it('should return false for input with only 2 characters (1 letter and 1 number)', () => {
      expect(isValid('a1')).toBe(false);
    });    
  });