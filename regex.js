function isValid(input) {
    const length = input.length;
    if (length < 8 || length > 10) {
      return false;
    }
    const letterCount = (input.match(/[A-Za-z]/g) || []).length;
    const digitCount = (input.match(/\d/g) || []).length;
  
    if (
      (letterCount === 1 && digitCount === 8) ||
      (letterCount === 1 && digitCount === 9) ||
      (digitCount === 8 && letterCount === 2) 
    ) {
      return true;
    }
    return false;
  }
  
module.exports = isValid;
